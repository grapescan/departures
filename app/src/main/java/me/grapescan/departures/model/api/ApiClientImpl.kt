package me.grapescan.departures.model.api

import com.google.gson.annotations.SerializedName
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import ru.gildor.coroutines.retrofit.await

class ApiClientImpl : ApiClient {

    private val apiService: TimetableApiService = Retrofit.Builder()
            .baseUrl("http://api.mobile.staging.mfb.io/")
            .client(OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        chain.proceed(chain.request().newBuilder()
                                .addHeader("X-Api-Authentication", "intervIEW_TOK3n")
                                .build())
                    }
                    .build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TimetableApiService::class.java)

    suspend override fun getTimetable(stationId: Int): Timetable = apiService.getTimetable(stationId).await().timetable

    interface TimetableApiService {
        @GET("/mobile/v1/network/station/{stationId}/timetable")
        fun getTimetable(@Path("stationId") stationId: Int): Call<TimetableResponse>
    }

    class TimetableResponse(@SerializedName("timetable") val timetable: Timetable)
}