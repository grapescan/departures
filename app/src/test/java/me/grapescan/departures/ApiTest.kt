package me.grapescan.departures

import kotlinx.coroutines.experimental.runBlocking
import me.grapescan.departures.model.api.ApiClientImpl
import org.junit.Assert.assertTrue
import org.junit.Test

class ApiTest {

    @Test
    fun testApiCall() {
        val apiClient = ApiClientImpl()
        runBlocking {
            val timetable = apiClient.getTimetable(10)
            assertTrue(timetable.departures.isNotEmpty())
        }
    }
}
