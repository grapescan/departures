package me.grapescan.departures

import me.grapescan.departures.model.api.Timetable
import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat

class DateFormatTest {

    val dateTime = Timetable.Departure.DateTime(1524678300, "GMT+02:00")
    private val dateFormat = SimpleDateFormat("HH:mm")

    @Test
    fun testFormatting() {
        Assert.assertTrue(dateTime.format(dateFormat) == "17:31")
    }

    @Test
    fun testDateFormattingWithTimezone() {
        val formattedWithoutTz = SimpleDateFormat("HH:mm").format(dateTime.timestamp)
        Assert.assertTrue(dateTime.format(dateFormat) != formattedWithoutTz)
    }
}
