package me.grapescan.departures.ui

import me.grapescan.departures.model.api.ApiClientImpl
import me.grapescan.departures.model.api.CachingTimetableRepository
import me.grapescan.departures.ui.main.MainPresenter

object Injector {
    private val apiClient by lazy { ApiClientImpl() }
    private val timetableRepository by lazy { CachingTimetableRepository(apiClient) }
    val presenter by lazy { MainPresenter(timetableRepository) }
}