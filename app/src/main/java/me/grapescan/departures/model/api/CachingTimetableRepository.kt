package me.grapescan.departures.model.api

import me.grapescan.departures.model.TimetableRepository

class CachingTimetableRepository(private val apiClient: ApiClient) : TimetableRepository {

    private val cache: MutableMap<Int, Timetable> = mutableMapOf()

    suspend override fun getTimetable(stationId: Int): Timetable {
        if (!cache.containsKey(stationId)) {
            cache[stationId] = apiClient.getTimetable(stationId)
        }
        return cache[stationId]!!
    }

}