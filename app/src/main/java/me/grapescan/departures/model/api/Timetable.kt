package me.grapescan.departures.model.api

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class Timetable(@SerializedName("departures") val departures: List<Departure>) {
    data class Departure(
            @SerializedName("datetime") val dateTime: DateTime,
            @SerializedName("line_code") val lineCode: String,
            @SerializedName("direction") val direction: String
    ) {
        data class DateTime(
                @SerializedName("timestamp") val timestamp: Long,
                @SerializedName("tz") val tz: String
        ) {
            fun format(dateFormat: SimpleDateFormat): String {
                dateFormat.timeZone = TimeZone.getTimeZone(tz)
                return dateFormat.format(timestamp)
            }
        }
    }
}