package me.grapescan.departures.ui.main

import android.annotation.SuppressLint
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import me.grapescan.departures.model.TimetableRepository
import me.grapescan.departures.model.api.Timetable
import me.grapescan.departures.ui.BusStop
import me.grapescan.departures.ui.Presenter
import java.text.SimpleDateFormat

class MainPresenter(private val repository: TimetableRepository) : Presenter<MainView>() {

    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("HH:mm")

    override fun attachView(viewParam: MainView) {
        super.attachView(viewParam)
        loadData()
    }

    private fun loadData() {
        launch(UI) {
            view?.showProgress()
            try {
                view?.showData(repository.getTimetable(10).departures.map { it.toBusStop() })
            } catch (exception: Throwable) {
                view?.showError(exception)
            }
        }
    }

    private fun Timetable.Departure.toBusStop(): BusStop = BusStop(
            lineCode,
            direction,
            dateTime.format(dateFormat))

    fun onRetryClick() = loadData()
}