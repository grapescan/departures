package me.grapescan.departures.ui

data class BusStop(
        val lineNumber: String,
        val direction: String,
        val departureTime: String
)