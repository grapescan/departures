package me.grapescan.departures.ui.main

import me.grapescan.departures.ui.BusStop

interface MainView {
    fun showProgress()
    suspend fun showData(data: List<BusStop>)
    fun showError(error: Throwable)
}