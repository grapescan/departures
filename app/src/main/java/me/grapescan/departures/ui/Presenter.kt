package me.grapescan.departures.ui

abstract class Presenter<T> {

    protected var view: T? = null

    open fun attachView(viewParam: T) {
        this.view = viewParam
    }

    open fun detachView() {
        view = null
    }
}