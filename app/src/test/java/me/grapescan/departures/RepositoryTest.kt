package me.grapescan.departures

import junit.framework.Assert
import kotlinx.coroutines.experimental.runBlocking
import me.grapescan.departures.model.TimetableRepository
import me.grapescan.departures.model.api.ApiClient
import me.grapescan.departures.model.api.CachingTimetableRepository
import me.grapescan.departures.model.api.Timetable
import org.junit.Before
import org.junit.Test

class RepositoryTest {

    private lateinit var repository: TimetableRepository
    private lateinit var apiClient: FakeApiClient

    @Before
    fun setUp() {
        apiClient = FakeApiClient()
        repository = CachingTimetableRepository(apiClient)
    }

    @Test
    fun testInitialApiCall() {
        Assert.assertTrue(apiClient.callsCount == 0)
        val timetable = runBlocking { repository.getTimetable(10) }
        Assert.assertTrue(apiClient.callsCount == 1)
        Assert.assertTrue(timetable == FakeApiClient.DATA)
    }

    @Test
    fun testCaching() {
        Assert.assertTrue(apiClient.callsCount == 0)
        runBlocking { repository.getTimetable(10) }
        Assert.assertTrue(apiClient.callsCount == 1)
        runBlocking { repository.getTimetable(10) }
        Assert.assertTrue(apiClient.callsCount == 1)
    }

    class FakeApiClient : ApiClient {

        companion object {
            val DATA = Timetable(
                    listOf(
                            Timetable.Departure(
                                    Timetable.Departure.DateTime(1524675600, "GMT+02:00"),
                                    "L026",
                                    "Bolzano"
                            )
                    )
            )
        }

        var callsCount = 0

        suspend override fun getTimetable(stationId: Int): Timetable {
            callsCount++
            return DATA
        }
    }
}