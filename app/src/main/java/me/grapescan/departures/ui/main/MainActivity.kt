package me.grapescan.departures.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

import me.grapescan.departures.R
import me.grapescan.departures.ui.BusStop
import me.grapescan.departures.ui.Injector

class MainActivity : AppCompatActivity(), MainView {

    private val presenter by lazy { Injector.presenter }
    private val listAdapter by lazy { BusStopAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        busStopList.adapter = listAdapter
        busStopList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        errorAction.setOnClickListener { presenter.onRetryClick() }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun showProgress() {
        errorGroup.visibility = View.GONE
        progress.visibility = View.VISIBLE
        busStopList.visibility = View.GONE
    }

    override suspend fun showData(data: List<BusStop>) {
        progress.visibility = View.GONE
        busStopList.visibility = View.VISIBLE
        errorGroup.visibility = View.GONE
        listAdapter.submitList(data)
    }

    override fun showError(error: Throwable) {
        errorGroup.visibility = View.VISIBLE
        progress.visibility = View.GONE
        busStopList.visibility = View.GONE
    }
}
