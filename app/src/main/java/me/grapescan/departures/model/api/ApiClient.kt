package me.grapescan.departures.model.api

interface ApiClient {
    suspend fun getTimetable(stationId: Int): Timetable
}