package me.grapescan.departures.model

import me.grapescan.departures.model.api.Timetable

interface TimetableRepository {
    suspend fun getTimetable(stationId: Int): Timetable
}