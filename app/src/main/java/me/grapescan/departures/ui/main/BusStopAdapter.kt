package me.grapescan.departures.ui.main

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_bus_stop.view.*
import me.grapescan.departures.R
import me.grapescan.departures.ui.BusStop

class BusStopAdapter : ListAdapter<BusStop, BusStopAdapter.BusStopViewHolder>(BusStopDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusStopViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_bus_stop, parent, false)
        return BusStopViewHolder(view)
    }

    override fun onBindViewHolder(holder: BusStopViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class BusStopViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val direction = itemView.busStopDirection
        private val lineNumber = itemView.busStopLineNumber
        private val departureTime = itemView.busStopDepartureTime

        fun bind(item: BusStop) {
            direction.text = item.direction
            lineNumber.text = item.lineNumber
            departureTime.text = item.departureTime
        }
    }

    class BusStopDiffCallback : DiffUtil.ItemCallback<BusStop>() {
        override fun areItemsTheSame(oldItem: BusStop?, newItem: BusStop?): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: BusStop?, newItem: BusStop?): Boolean = oldItem == newItem
    }
}